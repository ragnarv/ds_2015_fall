import os
import subprocess
import sys
import time
import shutil

THISDIR = os.path.dirname(__file__)
PORT = 8874

try:
    sys.path.append(os.path.join(os.environ.get("SUMO_HOME", os.path.join(
        THISDIR, "..", "..", "..")), "tools")) 

    import traci
    from sumolib import checkBinary
except ImportError:
    sys.exit(
        "please declare environment variable 'SUMO_HOME' as the root directory of your sumo installation (it should contain folders 'bin', 'tools' and 'docs')")

		
def run():
    """execute the TraCI control loop"""
    traci.init(PORT)

    # main loop. do something every simulation step until no more vehicles are
    # loaded or running
    while traci.simulation.getMinExpectedNumber() > 0:
        traci.simulationStep()
		#Do something useful
        
    sys.stdout.flush()
    traci.close()

#Main entry
if __name__ == "__main__":
	sumoBinary = checkBinary('sumo')
	# run simulation
	retcode = subprocess.call(
		[sumoBinary, "-c", "data/cfg/dublinsim.sumocfg", "--no-step-log"], stdout=sys.stdout, stderr=sys.stderr)
	print ">> Simulation closed with status %s" % retcode
	sys.stdout.flush()
	
	run()
	sumoProcess.wait()
